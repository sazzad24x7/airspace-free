<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>">
        <meta name="description" content="Aviato E-Commerce Template">

        <meta name="author" content="Themefisher.com">



        <!-- Mobile Specific Meta-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon -->
        
        <!-- bootstrap.min css -->

        <!-- Ionic Icon Css -->
        <!-- animate.css -->

        <!-- Magnify Popup -->

        <!-- Owl Carousel CSS -->


        <!-- Main Stylesheet -->

        <?php wp_head();?>
    </head>

    <body id="body"<?php body_class();?> >

        <!-- Header Start -->
        <header class="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                       
                        
                        <!-- header Nav Start -->
                        <nav class="navbar">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="<?php bloginfo('url');?>">
                                        <?php the_custom_logo();?>
                                    </a>
                                </div>
                                <!-- Collect the nav links, forms, and other content for toggling -->
                                    <!-- wp_nav_menu()-->
                                    
                                    <?php
                                    wp_nav_menu(array(
                                        'theme_location' => 'top_menu',
                                        'container' => 'div',
                                        'container_class' => 'collapse navbar-collapse',
                                        'container_id'		=> 'bs-example-navbar-collapse-1',
                                        'menu_class' => 'nav navbar-nav navbar-right',
                                        'depth' => '3',
                                        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                                        'walker' =>  new WP_Bootstrap_Navwalker()
                                    ));
                                    ?> 
                                   
                            </div><!-- /.container-fluid -->
                        </nav>
                        
                        
                    </div>
                </div>
            </div>
        </header><!-- header close -->
