<?php
    function airspacefree_cs_js(){
    
    wp_enqueue_style('themeStyle_css',get_template_directory_uri().'/css/style.css',array(),'1.0','all');  
    wp_enqueue_style('bs_min_css',get_template_directory_uri().'/plugins/bootstrap/css/bootstrap.min.css',array(),'1.0','all');  
    wp_enqueue_style('Ioni_Icon_css',get_template_directory_uri().'/plugins/Ionicons/css/ionicons.min.css',array(),'1.0','all');  
    wp_enqueue_style('animate_css',get_template_directory_uri().'/plugins/animate-css/animate.css',array(),'1.0','all');  
    wp_enqueue_style('magnify_pop_css',get_template_directory_uri().'/plugins/magnific-popup/dist/magnific-popup.css',array(),'1.0','all');  
    wp_enqueue_style('owl_carousel_css',get_template_directory_uri().'/plugins/slick-carousel/slick/slick.css',array(),'1.0','all');  
    wp_enqueue_style('owl_carousel_css_theme',get_template_directory_uri().'/plugins/slick-carousel/slick/slick-theme.css',array(),'1.0','all'); 
        
    wp_enqueue_style('main-style',get_template_directory_uri().'/style.css',array(),'1.0','all');
        
     /*Essential Scripts*/  
    wp_enqueue_script('main_jQuery',esc_url('https://code.jquery.com/jquery-git.min.js'),array(),'1.0',true);
    wp_enqueue_script('bs_js',get_template_directory_uri().'/plugins/bootstrap/js/bootstrap.min.js',array(),'1.0',true);
    wp_enqueue_script('owl_c_js',get_template_directory_uri().'/plugins/slick-carousel/slick/slick.min.js',array(),'1.0',true);
    wp_enqueue_script('pop_js',get_template_directory_uri().'/plugins/magnific-popup/dist/jquery.magnific-popup.min.js',array(),'1.0',true);
    wp_enqueue_script('mix_js',get_template_directory_uri().'/plugins/mixitup/dist/mixitup.min.js',array(),'1.0',true);
wp_enqueue_script('timer_js',get_template_directory_uri().'/plugins/SyoTimer/build/jquery.syotimer.min.js',array(),'1.0',true);
    wp_enqueue_script('js_js',get_template_directory_uri().'/js/script.js',array(),'1.0',true);
    
 wp_enqueue_script('form_v',esc_url('http://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js'),array(),'1.0',true);
wp_enqueue_script('form_v_2',esc_url('http://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js'),array(),'1.0',true);
 
wp_enqueue_script('g_map',get_template_directory_uri().'/plugins/google-map/map.js',array(),'1.0',true);
wp_enqueue_script('google_map',esc_url('https://maps.googleapis.com/maps/api/js?key=AIzaSyAkeLMlsiwzp6b3Gnaxd86lvakimwGA6UA&callback=initMap'),array(),'1.0',true);
    }
add_action('wp_enqueue_scripts','airspacefree_cs_js');
?>