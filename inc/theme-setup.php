<?php 
function airspacefree_aftersetup(){
    add_theme_support('title-tag');
    add_theme_support('custom-logo');
    add_theme_support('custom-header');
    add_theme_support('custom-background');
    add_theme_support('post-thumbnails');
    
    register_nav_menus( array(
        'top_menu' => 'Page TOP Menu',
        'footer_menu' => 'Custom Footer Menu',
    ) );
    
    
    register_sidebar( array(
        'name' => __( 'Right Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

}
add_action('after_setup_theme','airspacefree_aftersetup');
add_action( 'widgets_init', 'airspacefree_aftersetup' );
?>




