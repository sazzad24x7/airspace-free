<!-- footer Start -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-manu">
                   <!-- <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">How it works</a></li>
                        <li><a href="#">Support</a></li>
                        <li><a href="#">Terms</a></li>
                    </ul>-->
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'footer_menu',
                        'container' => 'div',
                        'container_class' => 'footer-manu',
                        'container_id'		=> '',
                        'menu_class' => '',
                        'depth' => '3',
                        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                        'walker' =>  new WP_Bootstrap_Navwalker()
                    ));
                    ?> 
                </div>
                <p class="copyright">Copyright 2018 &copy; Design & Developed by <a href="http://www.themefisher.com">themefisher.com</a>. All rights reserved.
                    <br>
                    Get More <a href="https://themefisher.com/free-bootstrap-templates/" target="_blank">Free Bootstrap Templates</a>
                </p>
            </div>
        </div>
    </div>
</footer>

<!-- 
Essential Scripts
=====================================-->

<!-- <script src="js/jquery.counterup.js"></script> -->

<!-- Main jQuery 

<script src="https://code.jquery.com/jquery-git.min.js"></script> -->
<!-- Bootstrap 3.1 -->

<!-- Owl Carousel -->

<!--  -->

<!-- Mixit Up JS -->

<!-- <script src="plugins/count-down/jquery.lwtCountdown-1.0.js"></script> -->



<!-- Form Validator -->




<!-- Google Map -->






<?php wp_footer();?>
</body>
</html>